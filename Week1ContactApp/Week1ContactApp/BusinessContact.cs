﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1ContactApp
{
    [Serializable]
    public class BusinessContact : Contact
    {
        public string Company { get; set; }
        public string FaxNumber { get; set; }
        
        public BusinessContact(string name, string phoneNumber, string company, string faxNumber)
        {
            this.Name = name;
            this.PhoneNumber = phoneNumber;
            this.Company = company;
            this.FaxNumber = faxNumber;
        }

        public BusinessContact()
        {

        }

        override
        public string ToString()
        {
            return String.Format("This fucking person has name: {0}\n" +
                                "And phone number is freaking: {1}\n" +
                                "Works in stupid: {2}\n" +
                                "HAHA: {3}\n\n"
                                ,this.Name, this.PhoneNumber, this.Company, this.FaxNumber);
        }
    }
}
