﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Week1ContactApp;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void PeronalContactCreateObject()
        {
            PersonalContact personalContact = new PersonalContact()
            {
                Name = "Jason Shin",
                Address = "Wyndham St",
                PhoneNumber = "0410344018"
            };

            Assert.AreEqual(personalContact.Name, "Jason Shin");
            Assert.AreEqual(personalContact.Address, "Wyndham St");
            Assert.AreEqual(personalContact.PhoneNumber, "0410344018");
        }
        
        [TestMethod]
        public void BusinessContactCreateObject()
        {
            BusinessContact businessContact = new BusinessContact()
            {
                Name = "Jason Shin",
                Company = "UTS",
                PhoneNumber = "0410344018",
                FaxNumber = "1111"
            };

            Assert.AreEqual(businessContact.Name, "Jason Shin");
            Assert.AreEqual(businessContact.PhoneNumber, "0410344018");
            Assert.AreEqual(businessContact.Company, "UTS");
            Assert.AreEqual(businessContact.FaxNumber, "1111");
        }

    }
}
